use anyhow::Result;

mod gdziepolek;

fn main() -> Result<()> {
    // println!("{:#?}", gdziepolek::get_offers());
    // println!("{:#?}", gdziepolek::get_pharmacies());
    // println!("{:#?}", gdziepolek::get_locations().map(|x| x.len()));

    let ids = vec![
        2602, 2619, 7233, 2542, 15155, 140, 2540, 2527, 2536, 2531, 2558, 2647, 4183, 2626, 2656,
        9399,
    ];
    let pharmacies = gdziepolek::get_locations()?
        .into_iter()
        .filter(|x| ids.contains(&x.Id));
    for pharmacy in pharmacies {
        println!("{:#?}", pharmacy);
    }

    // let encoded = "";
    // println!("{}", gdziepolek::decode_to_json(encoded)?);

    Ok(())
}
